console.log("Hello World")

let wweStars = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"]
console.log(wweStars)

function addWweStars(addIndex) {
	wweStars[wweStars.length++] = addIndex
}

addWweStars("John Cena")
console.log(wweStars)

/*----------------------*/

function getWweStars(getIndex){
	return wweStars[getIndex]
}

let itemFound = getWweStars(2)
console.log(itemFound + " found.")

/*----------------------*/

function deleteWweStars(){
	let lastElementIndex = wweStars[wweStars.length - 1]
	wweStars.pop()
	return lastElementIndex

}

let lastElementIndex = deleteWweStars()
console.log(lastElementIndex + " will be deleted.")
console.log(wweStars)



/*-----------------------*/

function updateWweStars(updateIndex, updateStars){
	wweStars[updateIndex] = updateStars
}

updateWweStars(3, "Triple H")
console.log(wweStars)

/*-------------------------*/

 function deleteAll(){
 	wweStars = [];
 }

 deleteAll();
 console.log(wweStars);

/*-------------------------*/

function checkEmptyArray(){
	if(wweStars.length > 0) {
		return false;
	} else {
		return true;
	}
}

let emptyArray = checkEmptyArray();
console.log(emptyArray)